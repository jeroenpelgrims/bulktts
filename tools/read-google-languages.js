Array.from(document.querySelectorAll('.language-list:first-child [class*="language_list_item_wrapper-"]')).map(e => {
  const match = e.className.match(/language_list_item_wrapper-(\w{2})/);
  const languageCode = match && match.length > 1 ? match[1] : null;
  const languageNode = e.querySelector(".language_list_item_language_name");
  const language = languageNode ? languageNode.innerHTML : null;

  if (languageCode && language && !/\d/.test(languageCode)) {
    console.log(languageCode);
    return [ languageCode, language ];
  }

  return null;
})
.filter(x => x !== null)
.reduce((result, [ languageCode, language ]) => ({...result, [languageCode]: language}), {});