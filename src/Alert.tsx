import * as React from "react";
import { Colors } from "./App";

type Props = {
  type: "info" | "warning" | "danger" | "success";
} & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

function getColor(type: Props["type"]) {
  switch (type) {
    case "info":
      return Colors.blue;
    case "warning":
      return Colors.yellow;
    case "danger":
      return Colors.red;
    case "success":
      return Colors.green;
  }
}

export default (props: Props = { type: "info", children: [] }) => {
  return (
    <div
      style={{
        ...props.style,
        padding: "1em",
        border: `1px solid ${getColor(props.type)}`,
        backgroundColor: `${getColor(props.type)}20`
      }}
    >
      {props.children}
    </div>
  );
};
