import express from 'express';
import fetch from 'node-fetch';
import path from 'path';

const app = express();

app.use(express.static(path.join(__dirname, '..', '..', 'build')));

app.get<{ languageCode: string, text: string }>(
  '/tts/:languageCode/:text',
  async (req, res) => {
    const { languageCode, text } = req.params;
    const response = await fetch(
      `https://translate.google.com/translate_tts?ie=UTF-8&tl=${languageCode}&client=tw-ob&q=${encodeURIComponent(text)}`
    );
    const buffer = await response.arrayBuffer();
    const unpackedBuffer = new Uint8Array(buffer);

    res.type('mp3');
    res.end(Buffer.from(unpackedBuffer), 'binary');
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`listening on port ` + port);
});