import * as React from "react";
import Alert from "./Alert";
import download from "downloadjs";
import JSZip from 'jszip';
import languages from './languages.json';

export const Colors = {
  red: "#FF595E",
  yellow: "#FFCA3A",
  green: "#8AC926",
  blue: "#1982C4",
  purple: "#6A4C93"
};

const style = {
  title: {
  },
  wrapper: {
    display: "flex",
    flexDirection: "column" as React.CSSProperties["flexDirection"],
    maxWidth: "750px",
    margin: "auto"
  },
  actionButton: {
    fontSize: "1.5em",
    marginTop: "1em",
    padding: "0.5em",
    marginBottom: "1em"
  }
};

async function downloadAudio(text: string, languageCode: string) {
  const response = await fetch(`/tts/${languageCode}/${encodeURIComponent(text)}`);
  return response.blob();
}

async function doConversion(
  text: string,
  language: string,
  lineSplit: boolean
) {
  if (lineSplit) {
    const texts = text.split('\n')
    var zip = new JSZip();

    for (const text of texts) {
      const audio = await downloadAudio(text, language);
      zip.file(text + '.mp3', audio, { binary: true });
    }

    const zipfile = await zip.generateAsync<"blob">({ type: "blob" });
    download(zipfile, "bulktts.zip", "application/zip");
  } else {
    const audio = await downloadAudio(text, language);
    download(audio, text + ".mp3", "audio/mpeg");
  }
}

export default function App() {
  const [lineSplit, setLineSplit] = React.useState(false);
  const [text, setText] = React.useState("");
  const [language, setLanguage] = React.useState("en");

  return (
    <div style={style.wrapper}>
      <h1 style={style.title}>BulkTTS</h1>

      <p>
        BulkTTS can help you convert text to speech.<br/>
        Just type any text below, select the language and click the download button.
      </p>

      <textarea
        style={{
          flex: 1,
          marginBottom: "1em",
          minHeight: "10em",
          padding: "1em"
        }}
        placeholder="Text to transform to audio"
        value={text}
        onChange={e => setText(e.target.value)}
      />
      <label style={{ marginBottom: "1em" }}>
        <input
          type="checkbox"
          checked={lineSplit}
          onChange={e => setLineSplit(e.target.checked)}
        />
        Transform each line separately
      </label>

      <div>
        This text is in:
        <select value={language} onChange={e => setLanguage(e.target.value)}>
          {Object.entries(languages).map(
            ([ code, language ]) => <option value={code}>{language}</option>
          )}
          {}
        </select>
      </div>

      <button
        style={style.actionButton}
        onClick={() => doConversion(text, language, lineSplit)}
        disabled={!text || text.length === 0}
      >
        Download text as {lineSplit ? "mp3's in a zip file" : "mp3"}
      </button>

      {lineSplit ? (
        <Alert type="info">
          When you choose to <strong>transform each line separately</strong>, instead of downloading a single mp3 file for the entire text, each
          line will be converted to a separate mp3 file and added to a zip file.
          <br />
          This zip file will then be downloaded.
        </Alert>
      ) : null}
    </div>
  );
}
